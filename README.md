# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ:
При помощи универсальных методов объекта - bind, call и apply
bind - привязывает новый контекст к методу, но не вызывает его.
call и apply - связывает и сразу вызывает. Разница между ними только в способе передачи параметров.

#### 2. Что такое стрелочная функция?
> Ответ:
более короткая запись функции, с небольшой разницей. Стрелочные функции не имеют своего контекста, а а используют внешний контекст.

#### 3. Приведите свой пример конструктора. 
```js
// Ответ:
function Person(firstName, lastName, age) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.age = age;
}

```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js

// 1. сохраняем ссылку на контекст
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(that.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();

//2. Bind
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }.bind(this), 1000);
    }
  }

  person.sayHello()

//3. стрелочная функция
    const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(()=> {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
  
```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
